# Suggested minimal imapd.conf
# See imapd.conf(5) for more information and more options

# Space-separated users who have admin rights for all services.
admins: $CYRUS_ADMIN_USER

###################################################################
## File, socket and DB location settings.
###################################################################

# Configuration directory
configdirectory: /data/config

# Directories for proc and lock files
proc_path: /run/cyrus/proc
mboxname_lockpath: /run/cyrus/lock

# Locations for DB files
# The following DB are recreated upon initialization, so should live in
# ephemeral storage for best performance.
duplicate_db_path: /run/cyrus/deliver.db
ptscache_db_path:  /run/cyrus/ptscache.db
statuscache_db_path: /run/cyrus/statuscache.db
tls_sessions_db_path: /run/cyrus/tls_sessions.db

# Which partition to use for default mailboxes
defaultpartition: default
partition-default: /data/partitions/default

# If sieveusehomedir is false (the default), this directory is searched
# for Sieve scripts.
sievedir: /data/sieve

###################################################################
## Important: KEEP THESE IN SYNC WITH cyrus.conf
###################################################################

lmtpsocket: /run/cyrus/socket/lmtp
idlesocket: /run/cyrus/socket/idle
notifysocket: /run/cyrus/socket/notify

# Syslog prefix. Defaults to cyrus (so logging is done as cyrus/imap
# etc.)
syslog_prefix: cyrus

###################################################################
## Server behaviour settings
###################################################################

# Space-separated list of HTTP modules that will be enabled in
# httpd(8).  This option has no effect on modules that are disabled at
# compile time due to missing dependencies (e.g. libical).
#
# Allowed values: caldav, carddav, domainkey, ischedule, rss
httpmodules: caldav carddav jmap admin prometheus

# If enabled, the partitions will also be hashed, in addition to the
# hashing done on configuration directories. This is recommended if one
# partition has a very bushy mailbox tree.
hashimapspool: true

virtdomains: userid
altnamespace: true
unixhierarchysep: true
allowanonymouslogin: no
defaultdomain: localhost
crossdomains: true

###################################################################
## User Authentication settings
###################################################################

# Allow plaintext logins by default (SASL PLAIN)
allowplaintext: yes
sasl_pwcheck_method: saslauthd

# If enabled, the SASL library will automatically create authentication
# secrets when given a plaintext password. Refer to SASL documentation
sasl_auto_transition: no

###################################################################
## SSL/TLS Options
###################################################################

# File containing the global certificate used for ALL services (imap,
# pop3, lmtp, sieve)
#tls_server_cert: /etc/ssl/certs/ssl-cert-snakeoil.pem
tls_server_cert: $PATH_CERTIFICATE_PUBLIC

# File containing the private key belonging to the global server
# certificate.
#tls_server_key: /etc/ssl/private/ssl-cert-snakeoil.key
tls_server_key: $PATH_CERTIFICATE_PRIVATE


# File containing one or more Certificate Authority (CA) certificates.
#tls_client_ca_file: /etc/ssl/certs/cyrus-imapd-ca.pem

# Path to directory with certificates of CAs.
tls_client_ca_dir: /etc/ssl/certs

# The length of time (in minutes) that a TLS session will be cached for
# later reuse.  The maximum value is 1440 (24 hours), the default.  A
# value of 0 will disable session caching.
tls_session_timeout: 1440
autocreate_quota: 0
autocreate_post: 1
autocreate_inbox_folders: Drafts|Sent Messages|Deleted Messages|Junk|Archive
autocreate_subscribe_folders: Drafts|Sent Messages|Deleted Messages|Junk|Archive

# Turn on rfc6154 Special-Use Mailboxes
specialusealways: 1
xlist-drafts:  Drafts
xlist-sent:    Sent Messages
xlist-trash:   Deleted Messages
xlist-junk:    Junk
xlist-archive: Archive

conversations: 1
conversations_db: twoskip
jmap_nonstandard_extensions: 1
failedloginpause: 1s
prometheus_enabled: 1
prometheus_need_auth: none
prometheus_update_freq: 10s
