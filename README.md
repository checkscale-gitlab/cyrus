# Cyrus IMAP

Custom Docker images for Cyrus IMAP.

## License

This program is free software and is distributed under [AGPLv3+ License](./LICENSE).
